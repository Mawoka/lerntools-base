# Contributing

Thank you for considering contributing to Lerntools.

Please follow this guidelines if contributing to this repository.

For orientation also take a look into the [wiki](https://codeberg.org/lerntools/base/wiki) (some parts are in German, some in English).

## Code of Conduct

It should be obvious, but you have to follow this simple rules with any contribution.

* Stay respectful while contributing.
* Personal attacks are not allowed.
* Discrimination based on race/ethnic group, gender, age, religion, disability or sexuality is not allowed.

## Issues (Bugs and feature requests)

Please use the search function on the [issues page](https://codeberg.org/lerntools/base/issues) first, to check if the issue was already created by someone else. You can [write new bug reports and feature requests](https://codeberg.org/lerntools/base/issues/new) in English or German. If you just have a question feel free to ask at [Mastodon](https://bildung.social/@lerntools) or via email: team @ lerntools.org.

For bugs, please 
* describe on what page/function the bug appears, 
* list the steps to reproduce the bug, 
* what the wrong behaviour was and 
* what you expected as correct behaviour.

For feature requests
* please describe the feature
* explain the background why you need that feature (This is really important! Please explain in detail how you like to use the feature or what problem you like to solve with it. It makes desicions how to design the details of that feature much easier if we know that background.)
* maybe reference other tools that have a similar feature
* if you can, it is maybe helpful to explain how you use Lerntools in general, so we can understand your use case better.

## Pull requests

It's great if you not just suggest the changes but actually do the work, so we just have to accept it. To make that as smooth as possible, please consider the following aspects:

* Pull request should (in general) be created with the `dev` branch as target.

### Commit Message 

* Use the present tense ("Add feature" not "Added feature")
* Limit the first line to 72 characters or less
* Reference related issues and pull requests
* Describe the changes, so users and administrators will understand it (not "fix bug in App.vue", better "Stop navigation bar from flickers")

### Code

* Half finished changes will not be merged. If you plan to further work on it wait until you are finished with the pull request or add "WIP:" in front of the title.
* The build command has to work without errors.
* Don't put code into comments. If code isn't needed anymore, delete it. If we like to look up the previous code we always can use the git history. There is no need keep it in comments.
* If you change the dependencies in the `package.json`, always run `yarn install` to add them to the `yarn.lock` file as well.
