var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var BruteForceFilterSchema=new Schema({
	login:					{type: String, unique: true, required: true, max: 100},
	numberFailedLogins: 	{type: Number, default: 0},
	blockedUntilTs:			{type: Number, default: 0}
});


module.exports=mongoose.model('BruteForceFilter', BruteForceFilterSchema );
