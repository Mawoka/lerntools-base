var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var ResetSchema=new Schema({
	email:		{type: String, required: true, max: 200},
	ticket:		{type: String, unique:true, required: true},
	created:	{type: Date, required: true}
});


module.exports=mongoose.model('Reset', ResetSchema );
