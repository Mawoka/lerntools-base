var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var UserSchema=new Schema({
	login:			{type: String, unique: true, required: true, max: 100},
	password:		{type: String, required: true},
	name:			{type: String, max: 100},
	email:			{type: String, unique: true, max: 100},
	comment:		{type: String, default: "", max: 1000},
	active:			{type: Boolean, default: false},
	lang:			{type: String, max:10, default:"de"},
	roles:			[{ type: String }],
	lastLogin:		{type: Date},
	expireInfos:	{type: Number, default: 0}
});


module.exports=mongoose.model('User', UserSchema );
